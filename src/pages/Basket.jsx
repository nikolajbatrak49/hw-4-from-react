import ProductList from "../components/product-list";
import { useSelector } from "react-redux";

export function Basket() {
  const products = useSelector(
    (state) => state.merchandise.productsInBasket
  );
  return products.length ? (
    <>
      <ProductList
        products={products}
        page="basket"
      />
    </>
  ) : (
    "Корзина пуста"
  );
}
