import React from "react";
import ProductList from "../components/product-list";
import { useSelector } from "react-redux";

export function Home() {
  const products = useSelector((state) => state.merchandise.products);
  return (
    <div>
      <ProductList
        products={products}
        page="home"
      />
    </div>
  );
}
