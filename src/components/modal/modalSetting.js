const modalSettings = [
  {
    modalId: "default",
    settings: {
      header: "",
      closeButton: false,
      text: (data) => data,
      actions: [
        {
          type: "submit",
          backgroundColor: "white",
          text: "",
        },
        {
          type: "cancel",
          backgroundColor: "white",
          text: "",
        },
      ],
    },
  },
  {
    modalId: "addToBasket",
    settings: {
      header: "Придбання товару",
      closeButton: true,
      text: (data) => `Бажаєте додати ${data} до кошика?`,
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "Так, перейти до корзини",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "Ні, додам пізніше",
        },
      ],
    },
  },
  {
    modalId: "removeFromBasket",
    settings: {
      header: "Видалення товару",
      closeButton: true,
      text: (data) => `Ви впевнені що хочете видалити ${data} з корзини?`,
      actions: [
        {
          type: "submit",
          backgroundColor: "orange",
          text: "Так!",
        },
        {
          type: "cancel",
          backgroundColor: "green",
          text: "Ні, нехай лишається",
        },
      ],
    },
  },
];

export default modalSettings;
