import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../redux/actions/modal";

import { useEffect, useState } from "react";

import { Button } from "../button";
import modalSettings from "./modalSetting";
import "../../styles/modal.scss";

function Modal() {
  const [settings, setSettings] = useState({});
  const { visible, modalId, submitFunction, data } = useSelector(
    (state) => state.modal
  );
  const dispatch = useDispatch();

  useEffect(() => {
    const modal = modalSettings.find((item) => item.modalId === modalId);
    setSettings(modal.settings);
  }, [modalId, visible]);

  const { header, closeButton, text, actions } = settings;
  return (
    <div
      className={visible ? "modal active" : "modal"}
      onClick={() => dispatch(closeModal())}
    >
      <div
        className={visible ? "modal-body active" : "modal-body"}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={"modal-header"}>
          {header ? <h2>{header}</h2> : null}
          {closeButton && (
            <Button
              text="X"
              backgroundColor="rgb(163, 35, 0)"
              onClick={() => dispatch(closeModal())}
            />
          )}
        </div>
        <div className={"modal-content"}>{text && text(data)}</div>
        <div className={"modal-footer"}>
          {actions &&
            actions.map((item, index) => (
              <Button
                text={item.text}
                onClick={
                  item.type === "submit"
                    ? () => {
                        submitFunction();
                        dispatch(closeModal());
                      }
                    : () => dispatch(closeModal())
                }
                key={Date.now() + index}
              />
            ))}
        </div>
      </div>
    </div>
  );
}

export default Modal;
