import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../redux/actions/modal";
import {
  addProductToBasket,
  addProductToFavorite,
  removeProductFromFavorite,
  removeProductFromBasket,
} from "../../redux/actions/merchandise";

import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";

import styles from "../../styles/product.module.scss";
import { Button } from "../button";

import favoriteStar from "../../img/favourites-star.png";
import favoriteStarBlack from "../../img/favourites-star-black.png";

export default function Product({ page, ...props }) {
  const [inBasket, setInBasket] = useState(false);
  const [inFavorite, setInFavorite] = useState(false);

  const products = useSelector((state) => state.merchandise);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    // перевірка продукту на обране
    const productInFavorite = [...products.productsInFavorite].find(
      (item) => item.article === props.product.article
    );
    productInFavorite && setInFavorite(true);

    // перевірка продукту чи в корзині
    const productInBasket = [...products.productsInBasket].find(
      (product) => product.article === props.product.article
    );
    productInBasket && setInBasket(true);
  }, [props.product, products.productsInBasket, products.productsInFavorite]);

  function addToFavorite() {
    dispatch(addProductToFavorite(props.product));
    setInFavorite(true);
  }

  function removeFromFavorite() {
    dispatch(removeProductFromFavorite(props.product.article));
    setInFavorite(false);
  }

  function addToBasket() {
    dispatch(addProductToBasket(props.product));
    setInBasket(true);
    navigate("basket"); //перехід в корзину
  }

  function removeFromBasket() {
    dispatch(removeProductFromBasket(props.product.article));
    setInBasket(false);
  }

  const { title, price, imgUrl, company } = props.product;
  return (
    <div className={styles.Product}>
      <div
        className={styles.Favorite}
        onClick={() => {
          inFavorite ? removeFromFavorite() : addToFavorite();
        }}
      >
        <img src={inFavorite ? favoriteStarBlack : favoriteStar} alt="" />
      </div>
      <div className={styles.imageWrapper}>
        <img src={imgUrl} alt="" />
      </div>
      <div>
        <h2>{title}</h2>
        {company ? <h4>Марка: {company}</h4> : null}
        <h3>Ціна: {price} грн</h3>
      </div>
      <Button
        text={
          (page === "basket" && "Видалити?") || inBasket
            ? "Додано в кошик"
            : "До кошика"
        }
        backgroundColor={inBasket ? "grey" : "orange"}
        onClick={() => {
          (page === "home" || page === "favorite") && !inBasket
            ? dispatch(openModal("addToBasket", addToBasket, title))
            : page === "basket" &&
              dispatch(
                openModal("removeFromBasket", removeFromBasket, title)
              );
        }}
      />
    </div>
  );
}

Product.propTypes = {
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};
