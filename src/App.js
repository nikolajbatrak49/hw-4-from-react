import { useDispatch, useSelector } from "react-redux";
import { fetchProductsArray } from "./redux/actions/merchandise";

import { useEffect } from "react";
import { NavLink, Route, Routes } from "react-router-dom";

import { Modal } from "./components/modal";
import { Basket, Favorite, Home } from "./pages";

import styles from "./styles/app.module.scss";
import favoriteIcon from "./img/favourites-star.png";
import basketIcon from "./img/basket.png";
import homeIcon from "./img/home.png";

function App() {
  const products = useSelector((state) => state.merchandise);
  const { productsInFavorite, productsInBasket } = products;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductsArray());
  }, [dispatch]);

  return (
    <div className={styles.App}>
      <nav className={styles.productCounts}>
        <NavLink to="/" className={styles.navigateIcon}>
          <img src={homeIcon} alt="Home" />
        </NavLink>
        <NavLink to="/favorites" className={styles.navigateIcon}>
          <img src={favoriteIcon} alt="Favorite" />
          <span className={styles.productCount}>
            {productsInFavorite.length}
          </span>
        </NavLink>
        <NavLink to="/basket" className={styles.navigateIcon}>
          <img src={basketIcon} alt="Basket" />
          <span className={styles.productCount}>{productsInBasket.length}</span>
        </NavLink>
      </nav>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route
          path="/favorites"
          element={<Favorite products={productsInFavorite} />}
        />
        <Route
          path="/basket"
          element={<Basket products={productsInBasket} />}
        />
      </Routes>
      <Modal />
    </div>
  );
}

export default App;
