import { modalTypes } from "../types";

export function openModal(modalId, submitFunction, data) {
  return {
    type: modalTypes.OPEN_MODAL,
    payload: {
      modalId,
      submitFunction,
      data,
    },
  };
}

export function closeModal() {
  return {
    type: modalTypes.CLOSE_MODAL,
  };
}