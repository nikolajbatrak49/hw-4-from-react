import { merchandiseTypes } from "../types";

!localStorage.getItem("productsInFavorite") &&
  localStorage.setItem("productsInFavorite", JSON.stringify([]));

!localStorage.getItem("productsInBasket") &&
  localStorage.setItem("productsInBasket", JSON.stringify([]));

const initialState = {
  products: [],
  productsInFavorite: JSON.parse(localStorage.getItem("productsInFavorite")),
  productsInBasket: JSON.parse(localStorage.getItem("productsInBasket")),
};

const getProductsInFavorite = () =>
  JSON.parse(localStorage.getItem("productsInFavorite"));

const getProductsInBasket = () =>
  JSON.parse(localStorage.getItem("productsInBasket"));

export function merchandiseReducer(state = initialState, action) {
  switch (action.type) {
    case merchandiseTypes.GET_ALL_PRODUCTS:
      return {
        ...state,
        products: action.payload.products,
      };
    case merchandiseTypes.ADD_PRODUCT_TO_FAVORITE:
      localStorage.setItem(
        "productsInFavorite",
        JSON.stringify([...getProductsInFavorite(), action.payload.product])
      );
      return {
        ...state,
        productsInFavorite: JSON.parse(
          localStorage.getItem("productsInFavorite")
        ),
      };
    case merchandiseTypes.REMOVE_PRODUCT_FROM_FAVORITE:
      const productInFavoriteIndex = getProductsInFavorite().findIndex(
        (product) => product.article === action.payload.article
      );
      localStorage.setItem(
        "productsInFavorite",
        JSON.stringify([
          ...getProductsInFavorite().slice(0, productInFavoriteIndex),
          ...getProductsInFavorite().slice(productInFavoriteIndex + 1),
        ])
      );
      return {
        ...state,
        productsInFavorite: JSON.parse(
          localStorage.getItem("productsInFavorite")
        ),
      };
    case merchandiseTypes.ADD_PRODUCT_TO_BASKET:
      localStorage.setItem(
        "productsInBasket",
        JSON.stringify([...getProductsInBasket(), action.payload.product])
      );
      return {
        ...state,
        productsInBasket: JSON.parse(localStorage.getItem("productsInBasket")),
      };
    case merchandiseTypes.REMOVE_PRODUCT_FROM_BASKET:
      const productInBasketIndex = getProductsInBasket().findIndex(
        (product) => product.article === action.payload.article
      );
      localStorage.setItem(
        "productsInBasket",
        JSON.stringify([
          ...getProductsInBasket().slice(0, productInBasketIndex),
          ...getProductsInBasket().slice(productInBasketIndex + 1),
        ])
      );
      return {
        ...state,
        productsInBasket: JSON.parse(localStorage.getItem("productsInBasket")),
      };
    default:
      return state;
  }
}
