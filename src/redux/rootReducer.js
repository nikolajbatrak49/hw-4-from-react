import { combineReducers } from "redux";
import { modalReducer as modal } from "./reducers/modal";
import { merchandiseReducer as merchandise } from "./reducers/merchandise";

export const rootReducer = combineReducers({merchandise, modal});
